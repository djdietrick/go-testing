# Go Testing

## Primeapp

Simple go command line application to demonstrate unit testing and std in/out capturing.

## Webapp

Sample HTTP application that demonstrates testing routes, forms, middleware, and mocking requests. The app simply logs users in with username "admin@example.com" and password "secret" and displays a profile page.

```bash
# Run all unit tests
go test -v ./...

# Get coverage
go test -cover . # output coverage to command line
go test -coverprofile=coverage.out # save to file
go tool cover -html=coverage.out # serve file to web browser

# Run integration tests
# Signified by comment on first line `//go:build integration`
go test -v -tags=integration ./...
```

### Dependencies

```bash

# Run docker for integration tests within test
go get -u github.com/ory/dockertest/v3

# JWT
go get github.com/golang-jwt/jwt/v4
```
