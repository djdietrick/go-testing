# Basic test scenarios

```bash
# Test everything in current directory
go test .

# Test everything in subdirectories
go test ./...
```

## Coverage

### To check coverage and generate file

```bash
# See coverage percentage
go test -cover .

# Generate coverage file
go test -coverprofile=coverage.out .

# View code coverage from file
go tool cover -html=coverage.out

# Run individual test
go test -run Test_alpha_isPrime

# Run groups of tests
go test -run Test_alpha
```