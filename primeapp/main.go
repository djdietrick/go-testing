package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

func main() {
	// print welcome message
	intro()

	// create channel to indicate when the user wants to quit
	quit := make(chan bool)

	// start go routine to read user input and run isPrime function
	go readUserInput(os.Stdin, quit)

	// block until quit channel gets a value
	<-quit

	// close the channel
	close(quit)

	// print goodbye message
	fmt.Println("Goodbye!")
}

func intro() {
	fmt.Println("Welcome to the Prime Number App!")
	fmt.Println("Enter a number (q to quit): ")
	prompt()
}

func prompt() {
	fmt.Print("-> ")
}

func readUserInput(in io.Reader, quit chan bool) {
	scanner := bufio.NewScanner(in)
	for {
		res, done := checkNumber(scanner)
		if done {
			quit <- true
			return
		}
		fmt.Println(res)
		prompt()
	}
}

func checkNumber(scanner *bufio.Scanner) (string, bool) {
	scanner.Scan()
	input := scanner.Text()
	if strings.EqualFold(input, "q") {
		return "", true
	}
	n, err := strconv.Atoi(input)
	if err != nil {
		return "Invalid input, please try again!", false
	}
	_, msg := isPrime(n)

	return msg, false
}

func isPrime(n int) (bool, string) {
	if n == 1 || n == 0 {
		return false, fmt.Sprintf("%d is not prime, by definition!", n)
	}
	if n < 0 {
		return false, "Negative numbers are not prime, by definition!"
	}

	for i := 2; i <= n/2; i++ {
		if (n % i) == 0 {
			return false, fmt.Sprintf("%d is not prime, it is divisible by %d", n, i)
		}
	}
	return true, fmt.Sprintf("%d is prime!", n)
}
