package main

import (
	"bufio"
	"bytes"
	"io"
	"os"
	"strings"
	"testing"
)

func Test_alpha_isPrime(t *testing.T) {
	primeTests := []struct {
		name     string
		num      int
		expected bool
		message  string
	}{
		{"prime", 7, true, "7 is prime!"},
		{"not prime", 8, false, "8 is not prime, it is divisible by 2"},
		{"zero", 0, false, "0 is not prime, by definition!"},
		{"one", 1, false, "1 is not prime, by definition!"},
		{"negative", -1, false, "Negative numbers are not prime, by definition!"},
	}

	for _, tt := range primeTests {
		t.Run(tt.name, func(t *testing.T) {
			actual, msg := isPrime(tt.num)
			if actual != tt.expected {
				t.Errorf("isPrime(%d): expected %t, actual %t", tt.num, tt.expected, actual)
			}
			if msg != tt.message {
				t.Errorf("isPrime(%d): expected %s, actual %s", tt.num, tt.message, msg)
			}
		})
	}
}

func Test_alpha_prompt(t *testing.T) {
	oldOut := os.Stdout

	r, w, _ := os.Pipe()

	os.Stdout = w

	prompt()

	_ = w.Close()

	os.Stdout = oldOut

	out, _ := io.ReadAll(r)

	if string(out) != "-> " {
		t.Errorf("prompt(): expected %s, actual %s", "-> ", string(out))
	}
}

func Test_intro(t *testing.T) {
	oldOut := os.Stdout

	r, w, _ := os.Pipe()

	os.Stdout = w

	intro()

	_ = w.Close()

	os.Stdout = oldOut

	out, _ := io.ReadAll(r)

	expected := "Welcome to the Prime Number App!\n" +
		"Enter a number (q to quit): \n" +
		"-> "

	if string(out) != expected {
		t.Errorf("intro(): expected %s, actual %s", expected, string(out))
	}
}

func Test_checkNumber(t *testing.T) {
	tests := []struct {
		name     string
		input    string
		expected string
	}{
		{"prime", "7", "7 is prime!"},
		{"not prime", "8", "8 is not prime, it is divisible by 2"},
		{"zero", "0", "0 is not prime, by definition!"},
		{"one", "1", "1 is not prime, by definition!"},
		{"negative", "-1", "Negative numbers are not prime, by definition!"},
		{"not a number", "a", "Invalid input, please try again!"},
		{"quit", "q", ""},
		{"decimal", "1.1", "Invalid input, please try again!"},
		{"empty", "", "Invalid input, please try again!"},
		{"greek", "α", "Invalid input, please try again!"},
		{"emoji", "😀", "Invalid input, please try again!"},
	}

	for _, e := range tests {
		t.Run(e.name, func(t *testing.T) {
			input := strings.NewReader(e.input)
			reader := bufio.NewScanner(input)

			res, _ := checkNumber(reader)
			if !strings.EqualFold(res, e.expected) {
				t.Errorf("checkNumber(): expected %s, actual %s", e.expected, res)
			}
		})
	}
}

func Test_readUserInput(t *testing.T) {
	quit := make(chan bool)

	var stdin bytes.Buffer
	stdin.Write([]byte("7\nq\n"))

	go readUserInput(&stdin, quit)
	<-quit
	close(quit)

}
