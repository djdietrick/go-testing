package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
	"webapp/pkg/data"
)

func Test_app_enableCORS(t *testing.T) {
	nextHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {})

	tests := []struct {
		name           string
		method         string
		expectedHeader bool
	}{
		{"preflight", "OPTIONS", true},
		{"get", "GET", false},
	}

	for _, tt := range tests {
		handlerToTest := app.enableCORS(nextHandler)

		req := httptest.NewRequest(tt.method, "/", nil)
		rr := httptest.NewRecorder()

		handlerToTest.ServeHTTP(rr, req)

		if tt.expectedHeader && rr.Header().Get("Access-Control-Allow-Credentials") == "" {
			t.Errorf("%s: expected header not set", tt.name)
		}

		if !tt.expectedHeader && rr.Header().Get("Access-Control-Allow-Credentials") != "" {
			t.Errorf("%s: expected header to not be set", tt.name)
		}
	}
}

func Test_app_authRequired(t *testing.T) {
	nextHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {})

	testUser := data.User{
		ID:        1,
		FirstName: "Admin",
		LastName:  "User",
		Email:     "admin@example.com",
	}

	tokens, _ := app.generateTokenPair(&testUser)

	tests := []struct {
		name       string
		token      string
		expectAuth bool
		setHeader  bool
	}{
		{"valid", fmt.Sprintf("Bearer %s", tokens.Token), true, true},
		{"no token", "", false, false},
		{"invalid token", fmt.Sprintf("Bearer %s", expiredToken), false, true},
	}

	for _, tt := range tests {
		handlerToTest := app.authRequired(nextHandler)

		req := httptest.NewRequest("GET", "/", nil)
		if tt.setHeader {
			req.Header.Set("Authorization", tt.token)
		}

		rr := httptest.NewRecorder()

		handlerToTest.ServeHTTP(rr, req)

		if tt.expectAuth && rr.Code == http.StatusUnauthorized {
			t.Errorf("%s: expected auth; got %d", tt.name, rr.Code)
		}

		if !tt.expectAuth && rr.Code != http.StatusUnauthorized {
			t.Errorf("%s: expected unauth; got %d", tt.name, rr.Code)
		}
	}
}
