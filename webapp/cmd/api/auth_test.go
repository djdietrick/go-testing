package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
	"webapp/pkg/data"
)

func Test_app_getTokenFromHeaderAndVerify(t *testing.T) {
	testUser := data.User{
		ID:        1,
		FirstName: "Admin",
		LastName:  "User",
		Email:     "admin@example.com",
	}

	tokens, _ := app.generateTokenPair(&testUser)

	tests := []struct {
		name          string
		token         string
		errorExpected bool
		setHeader     bool
		issuer        string
	}{
		{"valid", fmt.Sprintf("Bearer %s", tokens.Token), false, true, app.Domain},
		{"valid but expired", fmt.Sprintf("Bearer %s", expiredToken), true, true, app.Domain},
		{"no header", fmt.Sprintf("Bearer %s", tokens.Token), true, false, app.Domain},
		{"invalid token", fmt.Sprintf("Bearer %s1", tokens.Token), true, true, app.Domain},
		{"no bearer", fmt.Sprintf("Bear %s", tokens.Token), true, true, app.Domain},
		{"three header parts", fmt.Sprintf("Bearer %s 1", tokens.Token), true, true, app.Domain},
		{"incorrect issuer", fmt.Sprintf("Bearer %s", tokens.Token), true, true, "example.org"},
	}

	for _, tt := range tests {
		if tt.issuer != app.Domain {
			app.Domain = tt.issuer
			tokens, _ = app.generateTokenPair(&testUser)
		}
		req, _ := http.NewRequest("GET", "/", nil)
		if tt.setHeader {
			req.Header.Set("Authorization", tt.token)
		}

		rr := httptest.NewRecorder()
		_, _, err := app.getTokenFromHeaderAndVerify(rr, req)
		if tt.errorExpected && err == nil {
			t.Errorf("%s: expected error; got nil", tt.name)
		}

		if !tt.errorExpected && err != nil {
			t.Errorf("%s: unexpected error %s", tt.name, err)
		}

		app.Domain = "example.com"
	}
}
