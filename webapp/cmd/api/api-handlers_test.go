package main

import (
	"context"
	"io"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"
	"time"
	"webapp/pkg/data"

	"github.com/go-chi/chi/v5"
)

func Test_app_authenticate(t *testing.T) {
	tests := []struct {
		name               string
		body               string
		expectedStatusCode int
	}{
		{"valid user", `{"email":"admin@example.com","password":"secret"}`, http.StatusOK},
		{"not json", `I'm not json`, http.StatusUnauthorized},
		{"empty json", `{}`, http.StatusUnauthorized},
		{"empty email", `{"email":""}`, http.StatusUnauthorized},
		{"empty password", `{"email":"admin@example.com"}`, http.StatusUnauthorized},
		{"invalid user", `{"email":"admin@someotherdomain.com","password":"secret"}`, http.StatusUnauthorized},
	}

	for _, tt := range tests {
		var reader io.Reader
		reader = strings.NewReader(tt.body)
		req, _ := http.NewRequest("POST", "/auth", reader)
		rr := httptest.NewRecorder()
		handler := http.HandlerFunc(app.authenticate)
		handler.ServeHTTP(rr, req)

		if tt.expectedStatusCode != rr.Code {
			t.Errorf("%s: expected %d; got %d", tt.name, tt.expectedStatusCode, rr.Code)
		}
	}
}

func Test_app_refresh(t *testing.T) {
	tests := []struct {
		name               string
		token              string
		expectedStatusCode int
		resetRefreshTime   bool
	}{
		{"valid", "", http.StatusOK, true},
		{"expired token", expiredToken, http.StatusBadRequest, false},
		{"valid but not ready to expire", "", http.StatusTooEarly, false},
	}

	testUser := data.User{
		ID:        1,
		FirstName: "Admin",
		LastName:  "User",
		Email:     "admin@example.com",
	}

	oldRefreshTime := refreshTokenExpiry

	for _, tt := range tests {
		var tkn string
		if tt.token == "" {
			if tt.resetRefreshTime {
				refreshTokenExpiry = time.Second * 1
			}
			tokens, _ := app.generateTokenPair(&testUser)
			tkn = tokens.RefreshToken
		} else {
			tkn = tt.token
		}

		postedData := url.Values{
			"refresh_token": {tkn},
		}

		req, _ := http.NewRequest("POST", "/refresh-token", strings.NewReader(postedData.Encode()))
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
		rr := httptest.NewRecorder()

		handler := http.HandlerFunc(app.refresh)
		handler.ServeHTTP(rr, req)

		if rr.Code != tt.expectedStatusCode {
			t.Errorf("%s: expected status %d; got %d", tt.name, tt.expectedStatusCode, rr.Code)
		}

		refreshTokenExpiry = oldRefreshTime
	}

}

func Test_app_userHandlers(t *testing.T) {
	tests := []struct {
		name           string
		method         string
		json           string
		paramID        string
		handler        http.HandlerFunc
		expectedStatus int
	}{
		{"all users", "GET", "", "", app.allUsers, http.StatusOK},
		{"delete user", "DELETE", "", "1", app.deleteUser, http.StatusNoContent},
		{"delete user bad url param", "DELETE", "", "y", app.deleteUser, http.StatusBadRequest},
		{"getUser valid", "GET", "", "1", app.getUser, http.StatusOK},
		{"getUser invalid", "GET", "", "2", app.getUser, http.StatusBadRequest},
		{"getUser invalid param", "GET", "", "y", app.getUser, http.StatusBadRequest},
		{"updateUser valid", "PATCH", `{"id":1,"first_name":"Administrator","last_name":"User","email":"admin@example.com"}`, "", app.updateUser, http.StatusNoContent},
		{"updateUser invalid", "PATCH", `{"id":2,"first_name":"Administrator","last_name":"User","email":"admin@example.com"}`, "", app.updateUser, http.StatusBadRequest},
		{"updateUser invalid json", "PATCH", `{"id":2,first_name:"Administrator","last_name":"User","email":"admin@example.com"}`, "", app.updateUser, http.StatusBadRequest},
		{"insertUser valid", "PUT", `{"first_name":"Jack","last_name":"Smith","email":"admin@example.com"}`, "", app.insertUser, http.StatusNoContent},
		{"insertUser invalid", "PUT", `{"foo":"bar","first_name":"Jack","last_name":"Smith","email":"admin@example.com"}`, "", app.insertUser, http.StatusBadRequest},
		{"insertUser invalid json", "PUT", `{first_name:"Jack","last_name":"Smith","email":"admin@example.com"}`, "", app.insertUser, http.StatusBadRequest},
	}

	for _, tt := range tests {
		var req *http.Request
		if tt.json == "" {
			req, _ = http.NewRequest(tt.method, "/users", nil)
		} else {
			req, _ = http.NewRequest(tt.method, "/users", strings.NewReader(tt.json))
		}

		if tt.paramID != "" {
			chiCtx := chi.NewRouteContext()
			chiCtx.URLParams.Add("userID", tt.paramID)
			req = req.WithContext(context.WithValue(req.Context(), chi.RouteCtxKey, chiCtx))
		}

		rr := httptest.NewRecorder()
		handler := http.HandlerFunc(tt.handler)
		handler.ServeHTTP(rr, req)

		if rr.Code != tt.expectedStatus {
			t.Errorf("%s: expected %d; got %d", tt.name, tt.expectedStatus, rr.Code)
		}
	}
}

func Test_app_refreshUsingCookie(t *testing.T) {
	testUser := data.User{
		ID:        1,
		FirstName: "Admin",
		LastName:  "User",
		Email:     "admin@example.com",
	}

	tokens, _ := app.generateTokenPair(&testUser)

	testCookie := &http.Cookie{
		Name:     "refresh_token",
		Path:     "/",
		Value:    tokens.RefreshToken,
		Expires:  time.Now().Add(refreshTokenExpiry),
		MaxAge:   int(refreshTokenExpiry.Seconds()),
		SameSite: http.SameSiteStrictMode,
		Domain:   "localhost",
		HttpOnly: true,
		Secure:   true,
	}

	badCookie := &http.Cookie{
		Name:     "refresh_token",
		Path:     "/",
		Value:    "somebadstring",
		Expires:  time.Now().Add(refreshTokenExpiry),
		MaxAge:   int(refreshTokenExpiry.Seconds()),
		SameSite: http.SameSiteStrictMode,
		Domain:   "localhost",
		HttpOnly: true,
		Secure:   true,
	}

	tests := []struct {
		name           string
		addCookie      bool
		cookie         *http.Cookie
		expectedStatus int
	}{
		{"valid", true, testCookie, http.StatusOK},
		{"invalid cookie", true, badCookie, http.StatusBadRequest},
		{"no cookie", false, nil, http.StatusUnauthorized},
	}

	for _, tt := range tests {
		rr := httptest.NewRecorder()
		req, _ := http.NewRequest("GET", "/", nil)

		if tt.addCookie {
			req.AddCookie(tt.cookie)
		}

		handler := http.HandlerFunc(app.refreshUsingCookie)
		handler.ServeHTTP(rr, req)

		if rr.Code != tt.expectedStatus {
			t.Errorf("%s: expected %d; got %d", tt.name, tt.expectedStatus, rr.Code)
		}
	}
}

func Test_app_deleteRefreshCookie(t *testing.T) {
	req, _ := http.NewRequest("GET", "/", nil)
	rr := httptest.NewRecorder()

	handler := http.HandlerFunc(app.deleteRefreshCookie)
	handler.ServeHTTP(rr, req)

	if rr.Code != http.StatusAccepted {
		t.Errorf("expected %d; got %d", http.StatusOK, rr.Code)
	}

	foundCookie := false
	for _, cookie := range rr.Result().Cookies() {
		if cookie.Name == "refresh_token" {
			foundCookie = true
			if cookie.Expires.After(time.Now()) {
				t.Error("expected cookie to be expired")
			}
		}
	}
	if !foundCookie {
		t.Error("expected cookie not found")
	}
}
