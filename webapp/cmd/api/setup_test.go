package main

import (
	"os"
	"testing"
	"webapp/pkg/repository/dbrepo"
)

var app application

var expiredToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhZG1pbiI6dHJ1ZSwiYXVkIjoiZXhhbXBsZS5jb20iLCJleHAiOjE3Mzc2NTA2ODYsImlzcyI6ImV4YW1wbGUuY29tIiwibmFtZSI6IkpvaG4gRG9lIiwic3ViIjoiMSJ9.BuOXhOd81kzwDE_jJR8TAP5GJKLrBjMhPii0d1WFXM4"

func TestMain(m *testing.M) {
	app.DB = &dbrepo.TestDBRepo{}
	app.Domain = "example.com"
	app.JWTSecret = "secret"

	os.Exit(m.Run())
}
