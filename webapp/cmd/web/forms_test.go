package main

import (
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
)

func TestForm_Has(t *testing.T) {
	form := NewForm(nil)

	has := form.Has("key")
	if has {
		t.Error("form should not have key")
	}

	postedData := url.Values{}
	postedData.Add("key", "value")

	form = NewForm(postedData)

	has = form.Has("key")
	if !has {
		t.Error("form should have key")
	}
}

func TestForm_Required(t *testing.T) {
	r := httptest.NewRequest("POST", "/whatever", nil)
	form := NewForm(r.PostForm)

	form.Required("a", "b", "c")
	if form.Valid() {
		t.Error("form should not be valid")
	}

	postedData := url.Values{}
	postedData.Add("a", "a")
	postedData.Add("b", "b")
	postedData.Add("c", "c")

	r, _ = http.NewRequest("POST", "/whatever", nil)
	r.PostForm = postedData

	form = NewForm(r.PostForm)
	form.Required("a", "b", "c")

	if !form.Valid() {
		t.Error("form should be valid")
	}

	postedData = url.Values{}
	postedData.Add("a", "a")
	postedData.Add("b", "b")

	r, _ = http.NewRequest("POST", "/whatever", nil)
	r.PostForm = postedData

	form = NewForm(r.PostForm)
	form.Required("a", "b", "c")

	if form.Valid() {
		t.Error("form should not be valid")
	}
}

func TestForm_Check(t *testing.T) {
	form := NewForm(nil)
	form.Check(false, "key", "message")
	if form.Valid() {
		t.Error("form should not be valid")
	}

}

func TestForm_ErrorGet(t *testing.T) {
	form := NewForm(nil)
	form.Check(false, "key", "message")

	s := form.Errors.Get("key")
	if len(s) == 0 {
		t.Error("should have error")
	}

	s = form.Errors.Get("notKey")
	if len(s) != 0 {
		t.Error("should not have error")
	}
}
