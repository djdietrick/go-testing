package main

import (
	"os"
	"testing"
	"webapp/pkg/repository/dbrepo"
)

var app application

func TestMain(m *testing.M) {
	pathToTemplates = "../../templates/"

	app = application{}

	app.Session = GetSession()

	app.DB = &dbrepo.TestDBRepo{}

	os.Exit(m.Run())
}
