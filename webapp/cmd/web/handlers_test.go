package main

import (
	"bytes"
	"context"
	"crypto/tls"
	"fmt"
	"image"
	"image/png"
	"io"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"path"
	"strings"
	"sync"
	"testing"
	"webapp/pkg/data"
)

func Test_application_handlers(t *testing.T) {
	var theTests = []struct {
		name                    string
		url                     string
		expectedStatusCode      int
		expectedURL             string
		expectedFirstStatusCode int
	}{
		{"home", "/", http.StatusOK, "/", http.StatusOK},
		{"404", "/not-exist", http.StatusNotFound, "/not-exist", http.StatusNotFound},
		{"profile", "/user/profile", http.StatusOK, "/", http.StatusTemporaryRedirect},
	}

	routes := app.routes()

	ts := httptest.NewTLSServer(routes)
	defer ts.Close()

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}

	client := &http.Client{
		Transport: tr,
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	for _, tt := range theTests {
		resp, err := ts.Client().Get(ts.URL + tt.url)
		if err != nil {
			t.Fatal(err.Error())
		}

		if resp.StatusCode != tt.expectedStatusCode {
			t.Errorf("for %s, expected %d but got %d", tt.name, tt.expectedStatusCode, resp.StatusCode)
		}

		if resp.Request.URL.Path != tt.expectedURL {
			t.Errorf("for %s, expected final url of %s but got %s", tt.name, tt.expectedURL, resp.Request.URL.Path)
		}

		resp2, _ := client.Get(ts.URL + tt.url)
		if resp2.StatusCode != tt.expectedFirstStatusCode {
			t.Errorf("for %s, expected first returned status code to be %d but got %d", tt.name, tt.expectedFirstStatusCode, resp2.StatusCode)
		}
	}
}

func Test_application_home(t *testing.T) {
	req, _ := http.NewRequest("GET", "/", nil)
	req = addContextAndSessionToRequest(req, app)

	rr := httptest.NewRecorder()

	handler := http.HandlerFunc(app.Home)
	handler.ServeHTTP(rr, req)

	if rr.Code != http.StatusOK {
		t.Errorf("home page status code is %d", rr.Code)
	}

	body, _ := io.ReadAll(rr.Body)
	if !strings.Contains(string(body), `<small>From session:`) {
		t.Errorf("home page body does not contain <small>From session:")
	}
}

func Test_application_home_new(t *testing.T) {
	tests := []struct {
		name         string
		putInSession string
		expectedHTML string
	}{
		{"first visit", "", "Session set"},
		{"second visit", "test", "From session: test"},
	}

	for _, tt := range tests {
		req, _ := http.NewRequest("GET", "/", nil)
		req = addContextAndSessionToRequest(req, app)
		_ = app.Session.Destroy(req.Context())

		if tt.putInSession != "" {
			app.Session.Put(req.Context(), "test", tt.putInSession)
		}

		rr := httptest.NewRecorder()

		handler := http.HandlerFunc(app.Home)
		handler.ServeHTTP(rr, req)

		if rr.Code != http.StatusOK {
			t.Errorf("home page status code is %d", rr.Code)
		}

		body, _ := io.ReadAll(rr.Body)
		if !strings.Contains(string(body), tt.expectedHTML) {
			t.Errorf("%s: body does not contain %s", tt.name, tt.expectedHTML)
		}
	}
}

func Test_application_render_with_bad_template(t *testing.T) {
	pathToTemplates = "./testdata/"

	req, _ := http.NewRequest("GET", "/", nil)
	req = addContextAndSessionToRequest(req, app)

	rr := httptest.NewRecorder()

	err := app.render(rr, req, "bad.page.gohtml", nil)
	if err == nil {
		t.Error("render should return an error for a bad template")
	}

	pathToTemplates = "../../templates/"
}

func getCtx(req *http.Request) context.Context {
	ctx := context.WithValue(req.Context(), contextUserKey, "unknown")
	return ctx
}

func addContextAndSessionToRequest(req *http.Request, app application) *http.Request {
	req = req.WithContext(getCtx(req))

	ctx, _ := app.Session.Load(req.Context(), req.Header.Get("X-Session"))
	return req.WithContext(ctx)
}

func Test_app_Login(t *testing.T) {
	tests := []struct {
		name               string
		postedData         url.Values
		expectedStatusCode int
		expectedLocation   string
	}{
		{
			name: "valid login",
			postedData: url.Values{
				"email":    {"admin@example.com"},
				"password": {"secret"},
			},
			expectedStatusCode: http.StatusSeeOther,
			expectedLocation:   "/user/profile",
		},
		{
			name: "missing form data",
			postedData: url.Values{
				"email":    {""},
				"password": {""},
			},
			expectedStatusCode: http.StatusSeeOther,
			expectedLocation:   "/",
		},
		{
			name: "bad credentials",
			postedData: url.Values{
				"email":    {"fake@example.com"},
				"password": {"1234"},
			},
			expectedStatusCode: http.StatusSeeOther,
			expectedLocation:   "/",
		},
		{
			name: "user not found",
			postedData: url.Values{
				"email":    {"admin2@example.com"},
				"password": {"secret"},
			},
			expectedStatusCode: http.StatusSeeOther,
			expectedLocation:   "/",
		},
		{
			name: "bad password",
			postedData: url.Values{
				"email":    {"admin@example.com"},
				"password": {"notright"},
			},
			expectedStatusCode: http.StatusSeeOther,
			expectedLocation:   "/",
		},
	}

	for _, tt := range tests {
		req, _ := http.NewRequest("POST", "/login", strings.NewReader(tt.postedData.Encode()))
		req = addContextAndSessionToRequest(req, app)
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

		rr := httptest.NewRecorder()
		handler := http.HandlerFunc(app.Login)
		handler.ServeHTTP(rr, req)

		if rr.Code != tt.expectedStatusCode {
			t.Errorf("%s: status code is %d, expected %d", tt.name, rr.Code, tt.expectedStatusCode)
		}

		actualLoc, err := rr.Result().Location()
		if err == nil {
			if actualLoc.String() != tt.expectedLocation {
				t.Errorf("%s: location is %s, expected %s", tt.name, actualLoc.String(), tt.expectedLocation)
			}
		} else {
			t.Errorf("%s: location is nil, expected %s", tt.name, tt.expectedLocation)
		}

	}
}

func Test_app_UploadFiles(t *testing.T) {
	pr, pw := io.Pipe()

	writer := multipart.NewWriter(pw)

	wg := &sync.WaitGroup{}
	wg.Add(1)

	go simulatePNGUpload("./testdata/img.png", writer, t, wg)

	request := httptest.NewRequest("POST", "/", pr)
	request.Header.Add("Content-Type", writer.FormDataContentType())

	uploadedFiles, err := app.UploadFiles(request, "./testdata/uploads/")
	if err != nil {
		t.Errorf("error uploading files: %s", err)
	}

	if _, err := os.Stat(fmt.Sprintf("./testdata/uploads/%s", uploadedFiles[0].OriginalFileName)); os.IsNotExist(err) {
		t.Errorf("uploaded file does not exist: %s", err)
	}

	err = os.Remove(fmt.Sprintf("./testdata/uploads/%s", uploadedFiles[0].OriginalFileName))
	if err != nil {
		t.Errorf("error removing uploaded file: %s", err)
	}

	wg.Wait()
}

func simulatePNGUpload(fileToUpload string, writer *multipart.Writer, t *testing.T, wg *sync.WaitGroup) {
	defer writer.Close()
	defer wg.Done()

	part, err := writer.CreateFormFile("file", path.Base(fileToUpload))
	if err != nil {
		t.Errorf("error creating form file: %s", err)
	}

	f, err := os.Open(fileToUpload)
	if err != nil {
		t.Errorf("error opening file: %s", err)
	}
	defer f.Close()

	img, _, err := image.Decode(f)
	if err != nil {
		t.Errorf("error decoding image: %s", err)
	}

	err = png.Encode(part, img)
	if err != nil {
		t.Errorf("error encoding image: %s", err)
	}
}

func Test_app_UploadProfilePic(t *testing.T) {
	uploadPath = "./testdata/uploads/"
	filePath := "./testdata/img.png"

	fieldName := "file"

	body := new(bytes.Buffer)

	mw := multipart.NewWriter(body)

	file, err := os.Open(filePath)
	if err != nil {
		t.Fatal(err)
	}

	w, err := mw.CreateFormFile(fieldName, filePath)
	if err != nil {
		t.Fatal(err)
	}

	if _, err = io.Copy(w, file); err != nil {
		t.Fatal(err)
	}

	mw.Close()

	req := httptest.NewRequest("POST", "/upload", body)
	req = addContextAndSessionToRequest(req, app)
	app.Session.Put(req.Context(), "user", data.User{ID: 1})
	req.Header.Add("Content-Type", mw.FormDataContentType())

	rr := httptest.NewRecorder()

	handler := http.HandlerFunc(app.UploadProfilePic)
	handler.ServeHTTP(rr, req)

	if rr.Code != http.StatusSeeOther {
		t.Errorf("status code is %d, expected %d", rr.Code, http.StatusSeeOther)
	}

	_ = os.Remove("./testdata/uploads/img.png")
}
